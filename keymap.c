/*
    Frosthaven's CRKBD Firmware
    reference: https://docs.qmk.fm/#/newbs_building_firmware
*/


#include QMK_KEYBOARD_H

/* RGB CUSTOM COLORS ***********************************************************
*******************************************************************************/

#define HSV_REDPINK 245,255,255
#define HSV_DEEPBLUE 150,255,255

#define PRIMARY_COLOR HSV_DEEPBLUE
#define SECONDARY_COLOR HSV_REDPINK
#define TERTIARY1_COLOR HSV_GREEN
#define TERTIARY2_COLOR HSV_ORANGE

/* KEYMAP **********************************************************************
https://docs.qmk.fm/#/keycodes_basic
https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/keycodes_us_ansi_shifted#keycodes
*******************************************************************************/

enum layers {
  _QWERTY,
  _DVORAK,
  _COLEMAK,
  _COLEMAK_DHM,
  _WORKMAN,
  //_GAMING,
  _FNSYMBOLS,
  _NUMBERS,
  _ADJUST
};

enum tap_dance {
  TD_FNSYMBOLS_CAPS,
  TD_ESC_TAB,
  TD_LALT_NLCK,
  TD_LGUI_APP
};

void TD_FNSYMBOLS_CAPS_finished(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        layer_on(_FNSYMBOLS);
    } else {
        register_code(KC_CAPS);
    }
}
void TD_FNSYMBOLS_CAPS_reset(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        layer_off(_FNSYMBOLS);
    } else {
        unregister_code(KC_CAPS);
    }
}

/*
void TD_NUMLOCK_NUMPAD_finished(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        layer_on(_NUMBERS);
    } else {
        register_code(KC_NLCK);
    }
}
void TD_NUMLOCK_NUMPAD_reset(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        layer_off(_NUMBERS);
    } else {
        unregister_code(KC_NLCK);
    }
}

bool gaming_mode_disabled = true;

void TD_GUI_GAMING_finished(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        register_code(KC_LGUI);
    } else {
        if (gaming_mode_disabled) {
            layer_off(_GAMING);
        } else {
            layer_on(_GAMING);
        }
    }
}
void TD_GUI_GAMING_reset(qk_tap_dance_state_t *state, void *user_data)
{
    if (state->count == 1) {
        unregister_code(KC_LGUI);
    } else {
        if (gaming_mode_disabled) {
            layer_on(_GAMING);
        } else {
            layer_off(_GAMING);
        }
        gaming_mode_disabled = !gaming_mode_disabled;
    }
}
*/

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_FNSYMBOLS_CAPS] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, TD_FNSYMBOLS_CAPS_finished, TD_FNSYMBOLS_CAPS_reset),
    //[TD_NUMLOCK_NUMPAD] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, TD_NUMLOCK_NUMPAD_finished, TD_NUMLOCK_NUMPAD_reset),
    //[TD_GUI_GAMING]     = ACTION_TAP_DANCE_FN_ADVANCED(NULL, TD_GUI_GAMING_finished, TD_GUI_GAMING_reset),
    [TD_ESC_TAB]        = ACTION_TAP_DANCE_DOUBLE(KC_TAB,KC_ESC),
    [TD_LALT_NLCK]      = ACTION_TAP_DANCE_DOUBLE(KC_LALT,KC_NLCK),
    [TD_LGUI_APP]       = ACTION_TAP_DANCE_DOUBLE(KC_LGUI,KC_APP)
};

/* LAYERS **********************************************************************
*******************************************************************************/

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_QWERTY] = LAYOUT_split_3x6_3(
      KC_TAB,   KC_Q,   KC_W,   KC_E,   KC_R,   KC_T,           KC_Y,   KC_U,  KC_I,     KC_O,    KC_P,     KC_BSPC,
      KC_LCTL,  KC_A,   KC_S,   KC_D,   KC_F,   KC_G,           KC_H,   KC_J,  KC_K,     KC_L,    KC_SCLN,  KC_QUOTE,
      KC_LSFT,  KC_Z,   KC_X,   KC_C,   KC_V,   KC_B,           KC_N,   KC_M,  KC_COMM,  KC_DOT,  KC_SLSH,  KC_RSFT,
                            KC_LALT, MO(_FNSYMBOLS), KC_SPC,           KC_ENT, MO(_NUMBERS), KC_LGUI
  ),

            // ALTERNATE LAYOUTS SELECTED FROM THE _ADJUST LAYER ///////////////
            [_DVORAK] = LAYOUT_split_3x6_3(
                _______, KC_QUOTE, KC_COMM, KC_DOT, KC_P, KC_Y,           KC_F, KC_G, KC_C, KC_R, KC_L, _______,
                _______, KC_A,     KC_O,    KC_E,   KC_U, KC_I,           KC_D, KC_H, KC_T, KC_N, KC_S, KC_SLSH,
                _______, KC_SCLN,  KC_Q,    KC_J,   KC_K, KC_X,           KC_B, KC_M, KC_W, KC_V, KC_Z, _______,
                                     _______, _______, _______,           _______, _______, _______
            ),
            [_COLEMAK] = LAYOUT_split_3x6_3(
                _______, KC_Q, KC_W, KC_F, KC_P, KC_G,           KC_J, KC_L, KC_U,    KC_Y,   KC_SCLN, _______,
                _______, KC_A, KC_R, KC_S, KC_T, KC_D,           KC_H, KC_N, KC_E,    KC_I,   KC_O,    KC_QUOTE,
                _______, KC_Z, KC_X, KC_C, KC_V, KC_B,           KC_K, KC_M, KC_COMM, KC_DOT, KC_SLSH, _______,
                            _______, _______, _______,           _______, _______, _______
            ),
            [_COLEMAK_DHM] = LAYOUT_split_3x6_3(
                _______, KC_Q, KC_W, KC_F, KC_P, KC_B,           KC_J, KC_L, KC_U,    KC_Y,   KC_SCLN, _______,
                _______, KC_A, KC_R, KC_S, KC_T, KC_G,           KC_M, KC_N, KC_E,    KC_I,   KC_O,    KC_QUOTE,
                _______, KC_Z, KC_X, KC_C, KC_D, KC_V,           KC_K, KC_H, KC_COMM, KC_DOT, KC_SLSH, _______,
                            _______, _______, _______,           _______, _______, _______
            ),
            [_WORKMAN] = LAYOUT_split_3x6_3(
                _______, KC_Q, KC_D, KC_R, KC_W, KC_B,           KC_J, KC_F, KC_U,    KC_P,   KC_SCLN, _______,
                _______, KC_A, KC_S, KC_H, KC_T, KC_G,           KC_Y, KC_N, KC_E,    KC_O,   KC_I,    KC_QUOTE,
                _______, KC_Z, KC_X, KC_M, KC_C, KC_V,           KC_K, KC_L, KC_COMM, KC_DOT, KC_SLSH, _______,
                            _______, _______, _______,           _______, _______, _______
            ),
            ////////////////////////////////////////////////////////////////////

  [_FNSYMBOLS] = LAYOUT_split_3x6_3(
      KC_ESC,                  KC_F1,   KC_F2,   KC_F3,   KC_F4,  KC_F5,      KC_GRV,  KC_EXLM, KC_AMPR,  KC_AT, KC_HASH,  KC_DEL,
      MT(MOD_LCTL, KC_CAPS),   KC_F6,   KC_F7,   KC_F8,   KC_F9,  KC_F10,     KC_DLR,  KC_LPRN, KC_LCBR,KC_LBRC, KC_PIPE, KC_CIRC,
      MT(MOD_LSFT, KC_NLCK),   KC_F11,  KC_F12,  KC_F13,  KC_F14, KC_F15,     KC_UNDS, KC_RPRN, KC_RCBR,KC_RBRC, KC_BSLS, KC_TILD,
                                 _______, _______, _______,     _______, MO(_ADJUST), KC_APP
  ),
  [_NUMBERS] = LAYOUT_split_3x6_3(
      KC_ESC,                KC_HOME,   KC_UP,  KC_END, XXXXXXX, KC_BTN2,     KC_PGUP, KC_KP_7, KC_KP_8, KC_KP_9, KC_PSLS, KC_PAST,
      MT(MOD_LCTL, KC_CAPS), KC_LEFT, KC_DOWN, KC_RGHT, KC_MS_U, KC_BTN1,     KC_PGDN,  KC_KP_4, KC_KP_5, KC_KP_6, KC_PMNS, KC_PLUS,
      MT(MOD_LSFT, KC_NLCK), KC_BTN2, XXXXXXX, KC_MS_L, KC_MS_D, KC_MS_R,     KC_KP_DOT, KC_KP_1, KC_KP_2, KC_KP_3, KC_PERC, KC_EQL,
                             _______, MO(_ADJUST), _______,     KC_PENT, _______, KC_KP_0
  ),
  [_ADJUST] = LAYOUT_split_3x6_3(
      KC_ESC,                KC_MSTP, KC_VOLU, KC_MPLY, RGB_VAI,  RGB_MOD,    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      MT(MOD_LCTL, KC_CAPS), KC_MPRV, KC_VOLD, KC_MNXT, RGB_VAD, RGB_RMOD,    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      MT(MOD_LSFT, KC_NLCK), XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,  RGB_TOG,    DF(_QWERTY), DF(_DVORAK), DF(_COLEMAK), DF(_COLEMAK_DHM), DF(_WORKMAN), XXXXXXX,
                                 _______, _______,  _______,    _______, _______, _______
  ),
};

/* STARTUP HACKS ***************************************************************
*******************************************************************************/

#ifdef AUTO_NUMLOCK

// enable numlock once on startup
// not working
void matrix_init_user (void) {
  if (!(host_keyboard_leds() & (1<<USB_LED_NUM_LOCK))) {
      register_code(KC_NUMLOCK);
      unregister_code(KC_NUMLOCK);
  }
}

#endif // AUTO_NUMLOCK

/* OLED ************************************************************************
*******************************************************************************/

#ifdef OLED_DRIVER_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_master) {
    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand
  }
  return rotation;
}

void oled_render_layer_state(void) {
  oled_write_P(PSTR("Layer: "), false);
  switch(biton32(layer_state)) {
    case _QWERTY:
      // @todo: reflect keyboard layout by name
      switch (biton32(default_layer_state)) {
        case _QWERTY:
          oled_write_ln_P(PSTR("QWERTY"), false);
          break;
        case _DVORAK:
          oled_write_ln_P(PSTR("DVORAK"), false);
          break;
        case _COLEMAK:
          oled_write_ln_P(PSTR("COLEMAK"), false);
          break;
        case _COLEMAK_DHM:
          oled_write_ln_P(PSTR("COLEMAK DHM"), false);
          break;
        case _WORKMAN:
          oled_write_ln_P(PSTR("WORKMAN"), false);
          break;
        default:
          oled_write_ln_P(PSTR("DEFAULT"), false);
          break;
      }
      break;
    case _NUMBERS:
        oled_write_ln_P(PSTR("NUMPAD & NAV"), false);
      break;
    case _FNSYMBOLS:
        oled_write_ln_P(PSTR("FN & SYMBOLS"), false);
      break;
    /*
    case _GAMING:
        oled_write_ln_P(PSTR("GAMING"), false);
        break;
    */
    case _ADJUST:
      oled_write_ln_P(PSTR("ADJUST"), false);
      break;
    default:
      oled_write_ln_P(PSTR("UNKNOWN"), false);
      break;
  }
}

char keylog_str[24] = {};

const char code_to_name[60] = {
    ' ', ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
    'R', 'E', 'B', 'T', '_', '-', '=', '[', ']', '\\',
    '#', ';', '\'', '`', ',', '.', '/', ' ', ' ', ' '};

void set_keylog(uint16_t keycode, keyrecord_t *record) {
  char name = ' ';
    if ((keycode >= QK_MOD_TAP && keycode <= QK_MOD_TAP_MAX) ||
        (keycode >= QK_LAYER_TAP && keycode <= QK_LAYER_TAP_MAX)) { keycode = keycode & 0xFF; }
  if (keycode < 60) {
    name = code_to_name[keycode];
  }

  // update keylog
  snprintf(keylog_str, sizeof(keylog_str), "%dx%d, k%2d : %c",
           record->event.key.row, record->event.key.col,
           keycode, name);
}

void oled_render_keylog(void) {
    oled_write(keylog_str, false);
}

void render_bootmagic_status(bool status) {
    /* Show Ctrl-Gui Swap options */
    static const char PROGMEM logo[][2][3] = {
        {{0x97, 0x98, 0}, {0xb7, 0xb8, 0}},
        {{0x95, 0x96, 0}, {0xb5, 0xb6, 0}},
    };
    if (status) {
        oled_write_ln_P(logo[0][0], false);
        oled_write_ln_P(logo[0][1], false);
    } else {
        oled_write_ln_P(logo[1][0], false);
        oled_write_ln_P(logo[1][1], false);
    }
}

void oled_render_logo(void) {
    static const char PROGMEM crkbd_logo[] = {
        0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94,
        0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4,
        0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
        0};
    oled_write_P(crkbd_logo, false);
}

void oled_task_user(void) {
    if (is_master) {
        oled_render_layer_state();
        oled_render_keylog();
    } else {
        oled_render_logo();
    }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
    set_keylog(keycode, record);
  }
  return true;
}

#endif // OLED_DRIVER_ENABLE

/* RGB LAYER COLORS ************************************************************
https://beta.docs.qmk.fm/using-qmk/hardware-features/lighting/feature_rgblight
*******************************************************************************/
#ifdef RGBLIGHT_ENABLE

void rgblight_sethsv_master(int h, int s, int v) {
    if (is_master) {
        rgblight_sethsv_noeeprom(h, s, v);
        // @todo : figure out how the fuck lighting is supposed to work
        
        // sethsv(SECONDARY_COLOR, (LED_TYPE *)&led[6]);
    }
}

void rgblight_sethsv_slave(int h, int s, int v) {
    if (!is_master) {
        rgblight_sethsv_noeeprom(h, s, v);
    }
}

void set_default_colors(void) {
    rgblight_sethsv_master(PRIMARY_COLOR);
    rgblight_sethsv_slave(SECONDARY_COLOR);
}

layer_state_t layer_state_set_user(layer_state_t state) {
    // layer colors
    switch(biton32(state)) {
    case _NUMBERS:
        rgblight_sethsv_noeeprom(TERTIARY2_COLOR);
        break;
    case _FNSYMBOLS:
        rgblight_sethsv_noeeprom(TERTIARY1_COLOR);
        break;
    case _ADJUST:
        rgblight_sethsv_noeeprom(HSV_WHITE);
        break;
    default:
        set_default_colors();
        break;
    }

    extern rgblight_config_t rgblight_config;
    if (is_master) {
        if (rgblight_config.enable) {
          rgblight_set_layer_state(0, layer_state_cmp(state, _QWERTY));
        } else {
          rgblight_set_layer_state(0, false);
        }
    } else {
        if (rgblight_config.enable) {
          rgblight_set_layer_state(1, layer_state_cmp(state, _QWERTY));
        } else {
          rgblight_set_layer_state(1, false);
        }
    }

    // indicator states
    /*
    if (is_master) {
        rgblight_set_layer_state(0, layer_state_cmp(state, _GAMING));
    } else {
        rgblight_set_layer_state(1, layer_state_cmp(state, _GAMING));
    }
    */

    return state;
}

#endif // RGBLIGHT_ENABLE

/* RGB INDICATOR LIGHTS ********************************************************
*******************************************************************************/

// HSV : Hue(cycles around ring),Saturation(how much color),Val(brightness)
/*
const rgblight_segment_t PROGMEM rgb_layer_caps[] = RGBLIGHT_LAYER_SEGMENTS(
    {0,  6, HSV_RED},   // starting at led 0, light 6 lights red (underglow)
    {6, 21, HSV_BLUE}  // starting at led 6, light 21 lights blue (keys)
);
*/

const rgblight_segment_t PROGMEM indicator_gamingleft[] = RGBLIGHT_LAYER_SEGMENTS(
    {6,1, SECONDARY_COLOR} // enter/space
);
const rgblight_segment_t PROGMEM indicator_gamingright[] = RGBLIGHT_LAYER_SEGMENTS(
    {6,1, PRIMARY_COLOR} // enter/space
);
const rgblight_segment_t PROGMEM indicator_capslock[] = RGBLIGHT_LAYER_SEGMENTS(
    {13,1, SECONDARY_COLOR} // center thumb cluster
);
const rgblight_segment_t PROGMEM indicator_numlock[] = RGBLIGHT_LAYER_SEGMENTS(
    {14,1, SECONDARY_COLOR} // outside thumb cluster
);

// Later layers will override earlier ones, and starts at 0
const rgblight_segment_t* const PROGMEM rgb_layers[] = RGBLIGHT_LAYERS_LIST(
    indicator_gamingleft,
    indicator_gamingright,
    indicator_capslock,
    indicator_numlock
);

bool led_update_user(led_t led_state) {
    if (is_master) {
        rgblight_set_layer_state(2, led_state.caps_lock);
        rgblight_set_layer_state(3, !led_state.num_lock);
    }
    return true;
}

/* DEFAULTS ********************************************************************
*******************************************************************************/

void keyboard_post_init_user(void) {
  // default color state
  rgblight_enable_noeeprom();
  set_default_colors();
  // rgblight_mode(RGBLIGHT_MODE_BREATHING);

  // light layer setup
  rgblight_layers = rgb_layers;

  // default spacebar indicators

  // auto numlock
  if (!(host_keyboard_leds() & (1<<USB_LED_NUM_LOCK))) {
    register_code(KC_NUMLOCK);
    unregister_code(KC_NUMLOCK);
  }

  extern rgblight_config_t rgblight_config;
  if (is_master) {
    rgblight_set_layer_state(0, true);
  } else {
    rgblight_set_layer_state(1, true);
  }
}



// // Turn lights on/of when computer powers on/off
// extern rgblight_config_t rgblight_config;
// extern backlight_config_t backlight_config;
// void suspend_power_down_user(void)
// {
//   // rgb
//   rgblight_config.enable = false;
//   rgblight_set();

//   // backlight
//   /** I don't know why, but 3 means "off" and down is up */
//   backlight_config.level = 3;
//   backlight_config.enable = false;
//   backlight_set(3);
// }

// void suspend_wakeup_init_user(void)
// {
//   rgblight_config.raw = eeconfig_read_rgblight();
//   backlight_config.raw = eeconfig_read_backlight();

//   backlight_set(backlight_config.level);
//   rgblight_set();
// }